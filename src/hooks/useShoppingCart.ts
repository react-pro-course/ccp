import { useState } from "react";
import { ProductIntCart, Product } from "../interface/interface";

const useShoppingCart = () => {
  const [shoppingCart, setShoppingCart] = useState<{
    [key: string]: ProductIntCart;
  }>({});

  const onProductCountChange = ({
    count,
    product,
  }: {
    count: number;
    product: Product;
  }) => {
    setShoppingCart((oldShoppingCart) => {
      const productInCart: ProductIntCart = oldShoppingCart[product.id] || {
        ...product,
        count: 0,
      };

      if (Math.max(productInCart.count + count, 0) > 0) {
        productInCart.count += count;
        return {
          ...oldShoppingCart,
          [product.id]: productInCart,
        };
      }

      const { [product.id]: toDelete, ...rest } = oldShoppingCart;
      return rest;

      // if (count === 0) {
      //   const { [product.id]: toDelete, ...rest } = oldShoppingCart;

      //   // delete oldShoppingCart[product.id];
      //   return rest;
      // }

      // const newShoppingCart = {
      //   ...oldShoppingCart,
      //   [product.id]: {
      //     ...product,
      //     count,
      //   },
      // };

      // return newShoppingCart;
    });
  };

  return {
    onProductCountChange,
    shoppingCart,
  };
};

export default useShoppingCart;
