import { ProductCard } from "../components";
import "../styles/custom-styles.css";
import { ProductImage } from "../components/ProductImage";
import { ProductTitle } from "../components/ProductTitle";
import { ProductButtons } from "../components/ProductButtons";
import { products } from "../data/products";
// import useShoppingCart from "../hooks/useShoppingCart";

export const ShoppingPage = () => {
  // const { onProductCountChange, shoppingCart } = useShoppingCart();

  return (
    <div>
      <h1>shopping Store</h1>
      <hr />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
        }}
      >
        <ProductCard
          product={products[0]}
          initialValues={{
            count: 4,
          }}
        >
          {({ count, isMaxCountReached, reset, increaseBy }) => (
            <>
              <ProductImage />
              <ProductTitle />
              <ProductButtons />
            </>
          )}
        </ProductCard>
      </div>
    </div>
  );
};
