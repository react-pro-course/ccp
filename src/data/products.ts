import { Product } from "../interface/interface";

const product1 = {
  id: "1",
  title: "Coffe - Mug",
  image: "./coffee-mug.png",
};

const product2 = {
  id: "2",
  title: "Coffe - Mug - Meme",
  image: "./coffee-mug2.png",
};

export const products: Product[] = [product1, product2];
